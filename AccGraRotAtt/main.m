//
//  main.m
//  AccGraRotAtt
//
//  Created by Developer on 11/7/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([TDAppDelegate class]));
  }
}
