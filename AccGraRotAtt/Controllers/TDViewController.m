//
//  TDViewController.m
//  AccGraRotAtt
//
//  Created by Developer on 11/7/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import "TDViewController.h"
#import "TDPointData.h"
#import "TDStepTracker.h"

#define kPositionUpdateFrequency (1.f/10.f)

@interface TDViewController()
//acceleration
@property (nonatomic, strong) IBOutlet UILabel * accXVal;
@property (nonatomic, strong) IBOutlet UILabel * accYVal;
@property (nonatomic, strong) IBOutlet UILabel * accZVal;
//gravity
@property (nonatomic, strong) IBOutlet UILabel * graXVal;
@property (nonatomic, strong) IBOutlet UILabel * graYVal;
@property (nonatomic, strong) IBOutlet UILabel * graZVal;
//rotation
@property (nonatomic, strong) IBOutlet UILabel * rotXVal;
@property (nonatomic, strong) IBOutlet UILabel * rotYVal;
@property (nonatomic, strong) IBOutlet UILabel * rotZVal;
//attitude
@property (nonatomic, strong) IBOutlet UILabel * pitchVal;
@property (nonatomic, strong) IBOutlet UILabel * rollVal;
@property (nonatomic, strong) IBOutlet UILabel * yawVal;
//steps
@property (nonatomic, strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic, strong) IBOutlet UILabel * stepsLabel;
@property (nonatomic, strong) IBOutlet UIButton * resetButton;

@property (nonatomic, strong) CMMotionManager * motionManager;
@property (nonatomic, strong) CADisplayLink * displayLink;
@property (nonatomic, assign) NSInteger steps;
@property (nonatomic, assign) BOOL isStarted;
@property (nonatomic, strong) TDPointData * previousPoint;

@end

@interface TDViewController ()

@end

@implementation TDViewController

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  _steps = 0;
  _isStarted = NO;
  [self displayStepsCount];
  [self labelPresets];
  if ([[TDStepTracker sharedInstance] isCoreMotionAvailable] == YES)
  {
    [[TDStepTracker sharedInstance] startCounting];
//    [self performSelector:@selector(updateMotionData) withObject:nil afterDelay:kPositionUpdateFrequency inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
    NSTimer *timer = [NSTimer timerWithTimeInterval:kPositionUpdateFrequency target:self selector:@selector(updateMotionData) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
  }
  else
  {
    [self updateMotionData];
  }
}

- (void)updateMotionData
{
  if ([[TDStepTracker sharedInstance] isCoreMotionAvailable] == YES)
  {
    TDPointData * currentPoint = [[TDStepTracker sharedInstance] getLastRegisteredPoint];
    if (self.previousPoint == nil)
    {
      self.previousPoint = [TDPointData new];
      self.previousPoint = currentPoint;
    }
    if ([TDPointData isOnePoint:currentPoint equalToAnotherPoint:self.previousPoint] == NO)
    {
      self.previousPoint = currentPoint;
      [self displayPosition:currentPoint];
      [self displayStepsCount];
    }
  }
  else
  {
    if (self.previousPoint == nil)
    {
      self.previousPoint = [TDPointData new];
    }
    [self displayPosition:self.previousPoint];
    [self displayStepsCount];
  }
}

- (void)displayPosition:(TDPointData *)point
{
  self.accXVal.text = [NSString stringWithFormat:@"%.3f", point.accelerationX];
  self.accYVal.text = [NSString stringWithFormat:@"%.3f", point.accelerationY];
  self.accZVal.text = [NSString stringWithFormat:@"%.3f", point.accelerationZ];
  self.graXVal.text = [NSString stringWithFormat:@"%.3f", point.gravityX];
  self.graYVal.text = [NSString stringWithFormat:@"%.3f", point.gravityY];
  self.graZVal.text = [NSString stringWithFormat:@"%.3f", point.gravityZ];
  self.rotXVal.text = [NSString stringWithFormat:@"%.3f", point.rotationX];
  self.rotYVal.text = [NSString stringWithFormat:@"%.3f", point.rotationY];
  self.rotZVal.text = [NSString stringWithFormat:@"%.3f", point.rotationZ];
  self.pitchVal.text = [NSString stringWithFormat:@"%.3f", point.pitch];
  self.rollVal.text = [NSString stringWithFormat:@"%.3f", point.roll];
  self.yawVal.text = [NSString stringWithFormat:@"%.3f", point.yaw];
}

- (IBAction)clickedResetButton:(id)sender
{
  [[TDStepTracker sharedInstance] resetSteps];
  [self displayStepsCount];
}

- (void)displayStepsCount
{
  self.steps = [[TDStepTracker sharedInstance] currentSteps];
  self.stepsLabel.text = [NSString stringWithFormat:@"%d steps", self.steps];
}

- (void)labelPresets
{
  NSArray * views = @[_accXVal,
                      _accYVal,
                      _accZVal,
                      _graXVal,
                      _graYVal,
                      _graZVal,
                      _rotXVal,
                      _rotYVal,
                      _rotZVal,
                      _pitchVal,
                      _rollVal,
                      _yawVal,
                      _scrollView,
                      _stepsLabel,
                      _resetButton
                      ];
  for (UIView * view in views)
  {
    view.layer.cornerRadius = 3.f;
    view.clipsToBounds = YES;
    view.layer.borderColor = [UIColor blueColor].CGColor;
    view.layer.borderWidth = 1.f;
    view.backgroundColor = [UIColor colorWithRed:0.8f green:0.98 blue:0.98f alpha:1.0];
  }
  _scrollView.backgroundColor = [UIColor colorWithRed:0.9f green:0.9 blue:0.9 alpha:1.0];
  _resetButton.backgroundColor = [UIColor colorWithRed:0.7f green:0.7 blue:0.8f alpha:1.0];
}

@end