//
//  TDStepTracker.h
//  AccGraRotAtt
//
//  Created by Developer on 12/18/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TDPointData;

@interface TDStepTracker : NSObject

+ (TDStepTracker *)sharedInstance;
- (void)startCounting;
- (void)stopCounting;
- (BOOL)isCoreMotionAvailable;
- (NSInteger)currentSteps;
- (NSInteger)currentTotalSteps;
- (TDPointData *)getLastRegisteredPoint;
- (void)resetSteps;
- (void)resetTotalSteps;

@end
