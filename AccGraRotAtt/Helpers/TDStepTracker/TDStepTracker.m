//
//  TDStepTracker.m
//  AccGraRotAtt
//
//  Created by Developer on 12/18/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import "TDStepTracker.h"
#import "TDPointData.h"
#define kCMDeviceMotionUpdateFrequency (1.f/60.f)
#define ACC_SENSITIVITY 0.3625f
#define GRA_POSITION_CONSTANT 0.7f
#define MIN_MEASUREMENT_INTERVAL_FOR_COUNTING 12

@interface TDStepTracker()

@property (nonatomic, assign) BOOL upStepRegistered;
@property (nonatomic, assign) BOOL downStepRegistered;

@property (nonatomic, strong) CMMotionManager * motionManager;
@property (nonatomic, strong) CADisplayLink * displayLink;
@property (nonatomic, assign) NSInteger steps;
@property (nonatomic, assign) NSInteger totalSteps;
@property (nonatomic, assign) NSInteger measurementInterval;
@property (nonatomic, assign) BOOL isStarted;
@property (nonatomic, strong) NSMutableArray * positionsArray;
@end

@implementation TDStepTracker

#pragma mark - self instances
- (CMMotionManager *)motionManager
{
  if(!_motionManager)
  {
    _motionManager = [CMMotionManager new];
    _motionManager.deviceMotionUpdateInterval = kCMDeviceMotionUpdateFrequency;
  }
  return _motionManager;
}

- (CADisplayLink *)displayLink
{
  if(!_displayLink)
  {
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateMotionData)];
  }
  return _displayLink;
}

- (TDPointData *)createPointWithAcceleration:(CMAcceleration)acceleration
                                     gravity:(CMAcceleration)gravity
                                    rotation:(CMRotationRate)rotation
                                    attitude:(CMAttitude *)attitude
{
  TDPointData * point = [TDPointData new];
  //set current Acceleration
  point.accelerationX = acceleration.x;
  point.accelerationY = acceleration.y;
  point.accelerationZ = acceleration.z;
  //set current Gravitation
  point.gravityX = gravity.x;
  point.gravityY = gravity.y;
  point.gravityZ = gravity.z;
  //set current Rotation
  point.rotationX = rotation.x;
  point.rotationY = rotation.y;
  point.rotationZ = rotation.z;
  //set current Attitude
  point.pitch = attitude.pitch;
  point.roll = attitude.roll;
  point.yaw = attitude.yaw;
  return point;
}

- (void)updateMotionData
{
  self.measurementInterval++;
  CMAcceleration acceleration = self.motionManager.deviceMotion.userAcceleration; //acceleration
  CMAcceleration gravity = self.motionManager.deviceMotion.gravity;               //gravity
  CMRotationRate rotation = self.motionManager.deviceMotion.rotationRate;         //rotation
  CMAttitude * attitude = self.motionManager.deviceMotion.attitude;               //attitude
  TDPointData * currentPosition = [self createPointWithAcceleration:acceleration gravity:gravity rotation:rotation attitude:attitude];
  TDPointData * lastPosition;
  if(self.positionsArray.count > 0)
  {
    lastPosition = [self.positionsArray lastObject];
    if ([self testGravityPosition:currentPosition.gravityX])
    {
      if ([self testStepUpByAccelerationCoordinate:(currentPosition.accelerationX) andPreviousAccelerationCoordinate:(lastPosition.accelerationX)])
      {
        [self registerStepUp];
        [self checkAndAddPosition:currentPosition];
      }
      else if ([self testStepDownByAccelerationCoordinate:(currentPosition.accelerationX) andPreviousAccelerationCoordinate:(lastPosition.accelerationX)])
      {
        [self registerStepDown];
        [self checkAndAddPosition:currentPosition];
      }
    }
    if([self testGravityPosition:currentPosition.gravityY])
    {
      if ([self testStepUpByAccelerationCoordinate:(currentPosition.accelerationY) andPreviousAccelerationCoordinate:(lastPosition.accelerationY)])
      {
        [self registerStepUp];
        [self checkAndAddPosition:currentPosition];
      }
      else if ([self testStepDownByAccelerationCoordinate:(currentPosition.accelerationY) andPreviousAccelerationCoordinate:(lastPosition.accelerationY)])
      {
        [self registerStepDown];
        [self checkAndAddPosition:currentPosition];
      }
    }
    if ([self testGravityPosition:currentPosition.gravityZ])
    {
      if ([self testStepUpByAccelerationCoordinate:currentPosition.accelerationZ andPreviousAccelerationCoordinate:lastPosition.accelerationZ])
      {
        [self registerStepUp];
        [self checkAndAddPosition:currentPosition];
      }
      else if ([self testStepDownByAccelerationCoordinate:currentPosition.accelerationZ andPreviousAccelerationCoordinate:lastPosition.accelerationZ])
      {
        [self registerStepDown];
        [self checkAndAddPosition:currentPosition];
      }
    }
  }
  else
  {
    [self checkAndAddPosition:currentPosition];
  }
}

- (void)registerStepDown
{
  if(self.upStepRegistered == YES && self.measurementInterval >= MIN_MEASUREMENT_INTERVAL_FOR_COUNTING)
  {
    self.steps++;
    self.totalSteps++;
    self.measurementInterval = 0;
  }
  self.upStepRegistered = NO;
  self.downStepRegistered = YES;
}

- (BOOL)testStepDownByAccelerationCoordinate:(float)currentCoordinate andPreviousAccelerationCoordinate:(float)previousCoordinate
{
  BOOL result = NO;
  if ((previousCoordinate - currentCoordinate) > ACC_SENSITIVITY)
  {
    result = YES;
  }
  return result;
}

- (void)registerStepUp
{
  self.upStepRegistered = YES;
  self.downStepRegistered = NO;
}

- (BOOL)testStepUpByAccelerationCoordinate:(float)currentCoordinate andPreviousAccelerationCoordinate:(float)previousCoordinate
{
  BOOL result = NO;
  if ((currentCoordinate - previousCoordinate) > ACC_SENSITIVITY)
  {
    result = YES;
  }
  return result;
}

- (BOOL)testGravityPosition:(float)coordinate
{
  BOOL result = NO;
  if (ABS(coordinate) > GRA_POSITION_CONSTANT)
  {
    result = YES;
  }
  return result;
}

- (void)checkAndAddPosition:(TDPointData*)position
{
  if (self.positionsArray.count > 2)
  {
    [self.positionsArray removeObjectAtIndex:0];
  }
  [self.positionsArray addObject:position];
}
#pragma mark - Public methods
+ (TDStepTracker *)sharedInstance
{
  static TDStepTracker *_stepTracker = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _stepTracker = [TDStepTracker new];
    _stepTracker.positionsArray = [NSMutableArray new];
    _stepTracker.upStepRegistered = NO;
    _stepTracker.downStepRegistered = NO;
    _stepTracker.steps = 0;
    _stepTracker.totalSteps = 0;
    _stepTracker.measurementInterval = 0;
    _stepTracker.isStarted = NO;
  });
  return _stepTracker;
}

- (void)startCounting
{
  self.positionsArray = [NSMutableArray new];
  self.upStepRegistered = NO;
  self.downStepRegistered = NO;
  self.steps = 0;
  self.totalSteps = 0;
  self.measurementInterval = 0;
  self.isStarted = NO;
  if ([self isCoreMotionAvailable] == YES)
  {
    [[self motionManager] startDeviceMotionUpdates];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  }
  else
  {
    [[[UIAlertView alloc] initWithTitle:@"Accelerometer alert!" message:@"There is no accelerometer device here!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
  }
}

- (void)stopCounting
{
  [[self motionManager] stopDeviceMotionUpdates];
}

- (BOOL)isCoreMotionAvailable
{
  return [self.motionManager isAccelerometerAvailable];
}

- (NSInteger)currentSteps
{
  return self.steps;
}

- (NSInteger)currentTotalSteps
{
  return self.totalSteps;
}

- (TDPointData *)getLastRegisteredPoint
{
  return [self.positionsArray lastObject];
}

- (void)resetSteps
{
  self.steps = 0;
}

- (void)resetTotalSteps
{
  self.totalSteps =
  self.steps =
  0;
}

@end
