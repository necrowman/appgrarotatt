//
//  TDCoreMotionData.h
//  AccGraRotAtt
//
//  Created by Developer on 11/15/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TDPointData : NSObject

@property (nonatomic, assign) float
  accelerationX,
  accelerationY,
  accelerationZ,
  gravityX,
  gravityY,
  gravityZ,
  rotationX,
  rotationY,
  rotationZ,
  pitch,
  roll,
  yaw;

+ (BOOL)isOnePoint:(TDPointData*)onePoint equalToAnotherPoint:(TDPointData*)anotherPoint;

@end
