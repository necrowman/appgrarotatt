//
//  TDkajshdf.m
//  AccGraRotAtt
//
//  Created by Developer on 11/15/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import "TDPointData.h"

@implementation TDPointData

- (id)init
{
  self = [super init];
  if (self)
  {
    self.accelerationX =
    self.accelerationY =
    self.accelerationZ =
    self.gravityX =
    self.gravityY =
    self.gravityZ =
    self.rotationX =
    self.rotationY =
    self.rotationZ =
    self.pitch =
    self.roll =
    self.yaw = 0.f;
  }
  return self;
}

+ (BOOL)isOnePoint:(TDPointData*)onePoint equalToAnotherPoint:(TDPointData*)anotherPoint
{
  BOOL result = YES;
  if (onePoint.accelerationX != anotherPoint.accelerationX ||
      onePoint.accelerationY != anotherPoint.accelerationY ||
      onePoint.accelerationZ != anotherPoint.accelerationZ ||
      onePoint.gravityX != anotherPoint.gravityX ||
      onePoint.gravityY != anotherPoint.gravityY ||
      onePoint.gravityZ != anotherPoint.gravityZ ||
      onePoint.rotationX != anotherPoint.rotationX ||
      onePoint.rotationY != anotherPoint.rotationY ||
      onePoint.rotationZ != anotherPoint.rotationZ ||
      onePoint.pitch != anotherPoint.pitch ||
      onePoint.roll != anotherPoint.roll ||
      onePoint.yaw != anotherPoint.yaw)
  {
    result = NO;
  }
  return result;
}

@end
