//
//  TDAppDelegate.h
//  AccGraRotAtt
//
//  Created by Developer on 11/7/13.
//  Copyright (c) 2013 TrueDevelopment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
